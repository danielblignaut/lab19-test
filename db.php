<?php    
    //escape column name entries?
    class DB {
        
        private  $conn;
        private static $db;

        
        public function __construct()
        {
            try 
            {
                Config::load();
                $this->conn = new PDO(Config::$database['driver'] .':host=' . Config::$database['host'] .';dbname=' . Config::$database['database'] .'', Config::$database['user'], Config::$database['password']);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }       
        }
        
        public function __destruct()
        {
            $this->disconnect();   
        }

        private function __clone() {}

        public static function & getInstance()
        {
            if(!self::$db)
            {
                self::$db = new DB;
            }

            return self::$db;
        }
        
        public static function disconnect()
        {
            $instance = self::getInstance();
            if($instance->conn != null)
                $instance->conn =null;
        }
        
        public static function runQuery($sql, $preparedStringValues = array(), $preparedIntValues = array())
        {
            try
            {
                $statement = self::getInstance()->conn->prepare($sql);
               
                if(isset($preparedIntValues))
                {
                    foreach($preparedIntValues as $key => $value)
                    {
                       $statement->bindValue(':' . $key, (int) ($value), PDO::PARAM_INT); 
                    }
                }
                
                if(isset($preparedStringValues))
                {
                    foreach ($preparedStringValues as $key=>$value) 
                    {
                        $statement->bindValue(':' . $key, $value, PDO::PARAM_STR);
                    }
                }

                $statement->execute();
                return $statement;
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }   
        }

        public static function getInsertId()
        {
            $instance = self::getInstance();
            return $instance->conn->lastInsertId();
        }
    }
?>