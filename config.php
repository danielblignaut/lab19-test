<?php

class Config {
 
    public static $database    = array();
 
    public static function load()
    {
        // Usually are loaded from config files saved somewhere
        self::$database    = array(
 
                                'driver'   => 'mysql',
                                'host'     => 'localhost',
                                'database' => 'test',
                                'user'     => 'root',
                                'password' => 'root'
                            );
    }
 
}