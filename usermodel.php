<?php
	class ValidationException extends Exception
	{

	}

	class UserModel extends Model
	{
		public static $table = 'users';
		public $name, $occupation,$email, $id;

		public function validateemail($email)
		{
			//check if email is valid.
			if(filter_var($email, FILTER_VALIDATE_EMAIL))
				//if it is return true.
			    return true;
			//otherwise it's invalid.
			else
			{
				//throw an error saying invalid email
			    throw new ValidationException("Error validating email: " . $email, 1);
			}   
		}
	}
?>