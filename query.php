<?php
	class Query
	{
		private $offset, $group, $limit, $order, $like, $join, $where, $select,$sql, $lastQuery, $preparedStringValues, $preparedIntValues, $table;
	
		public function __construct($table)
		{
			$this->setTable($table);
		}

		public function setTable($table)
		{
			try
			{
				if($this->checkTable($table))
				{
					$this->table = $table;
				}
			}
			catch(Exception $e)
			{
				echo $e->getMessage();
			}
		}

		private function isAssoc($arr)
        {
            return array_keys($arr) !== range(0, count($arr) - 1);
        }

		private function resetVariables()
        {
        	if(isset($this->sql))
        		$this->lastQuery = $this->sql;
            $this->group = $this->offset =  $this->limit = $this->order = $this->like = $this->join = $this->where = $this->select = $this->preparedStringValues = $this->preparedIntValues = null;
        }

        public function order($key, $dir)
        {
            $this->order = ' BY ' . $key . ' ' . strtoupper($dir);    
        }
        
        public function limit($start, $end = null)
        {
            $this->limit = $start;
            
            if(isset($end))
            {
                $this->limit .= ', ' . $end;
            }
        }
        
        private function prepareWhereTerm($column, $value, $sign = '=')
        {
            if(isset($this->where))
            {
                $this->where .= ' AND ';
            }
            
            $this->where .= $column . ' ' . $sign . ' ' . $this->addToPreparedValues($value);
        }

        public function offset($offset)
        {
            $this->offset = $offset;
        }
        
        public function where($values)
        {
            if(is_array($values))
            {   
                for($i=0; $i< sizeof($values); $i ++)
                {
                    if(isset($values['sign']))
                    {
                        $sign = $values['sign'];
                    }
                    else
                    {
                        $sign = '=';
                    }

                    $this->prepareWhereTerm(key($values), $values[key($values)], $sign);
                    
                    next($values);
                }
            }
            else if(is_string($values))
            {
                //needs fixing.
                if(!isset($this->where))
                {
                     $this->where = $values;   
                }
                else
                {
                     $this->where .= ' AND ' . $values;   
                }
            }           
        }
        
        public function join($table, $on)
        {            
            $this->join = ' JOIN ' . $table . ' ON ' . $on;
        }
        
        private function prepareLikeTerm($column, $value, $position = null)
        {
            if(isset($this->like))
            {
                $this->like .= ' AND ';  
            } 
            
            $this->like .= $column . ' LIKE ';
            $likeTerm = '';
            
            if($position == 'end' || $position == 'both' || !isset($position))
            {
                $likeTerm .= '%';
            }
            
            $likeTerm .= $value;
            
            if($position == 'start' || $position == 'both' || !isset($position))
            {
                $likeTerm .= '%';
            }
            
            
            $this->like .= $this->addToPreparedValues($likeTerm);
        }
        
        public function like($array, $value = null, $position = null)
        {
            if(is_array($array))
            {
                if(isset($array[0]))
                {
                    if($this->isAssoc($array[0]))
                    {
                        for($i=0; $i< sizeof($array); $i ++)
                        {
                            $this->prepareLikeTerm(key($array[$i]), $array[$i][key($array[$i])], $array[$i]['position']);
                        }
                    }
                }
                else
                {
                    if($this->isAssoc($array))
                    {
                        $this->prepareLikeTerm(key($array), $array[key($array)], $array['position']);
                    }
                }
            }
            else if(is_string($array) && is_string($value))
            {
                $this->prepareLikeTerm($array, $value, $position);
            }
            else if(is_string($array))
            {
                $array = preg_split('/like/i', $array);
 
                if(sizeof($array) == 2)
                {
                    //have to convert array to string to check individual characters
                    $string = $array[1];

                    if($string[1] == '%' && $string[strlen($array[1])-1] == '%')
                    {
                        $position = 'both';   
                        $value = substr($string, 2, strlen($string) -3);
                    }
                    else if($string[1] == '%')
                    {
                        $position = 'end'; 
                        $value = substr($string, 2);
                    }
                    else
                    {
                        $position = 'start';  
                        $value = substr($string, 1, strlen($string) -2);
                    }
                    
                    $this->prepareLikeTerm($array[0], $value, $position);
                }
            } 
        }
        
        public function select($values)
        {
            if(is_array($values))
            {
                for($i =0; $i<sizeof($values); $i++)
                {
                    $this->select[] =  $values[$i];
                }
            }
            else if(is_string($values))
            {
                $this->select(explode(',', trim($values)));
            }
        }

        public function selectSpecial($value, $function, $as = null)
        {
            $select = $function . '(' . $value . ')';

            if(isset($as))
            {
                $select .= ' AS \'' . $as . '\'';
            }

            $this->select($select);
        }

        public function selectAverage($value, $as = null)
        {
            $this->selectSpecial($value, 'AVG', $as);
        }

        public function selectMax($value, $as = null)
        {
            $this->selectSpecial($value, 'MAX', $as);
        }

        public function selectMin($value, $as = null)
        {
            $this->selectSpecial($value, 'MIN', $as);
        }

        public function selectCount($value, $as = null)
        {
            $this->selectSpecial($value, 'COUNT', $as);
        }
        
        public function groupBy($values)
        {
            if(is_string($values))
            {
                $this->prepareGroupBy($values);
            }
            else if(is_array($values))
            {
                for($i =0; $i<sizeof($values);$i++)
                {
                    $this->prepareGroupBy($values[$i]);
                }
            }
        }
        
        private function prepareGroupBy($column)
        {
            $this->group[] = $column;   
        }
        
        private function getSqlSelect()
        {
            $statement = 'SELECT ';
            
            if(sizeof($this->select) == 0 )
            {
                $statement .= '* ';
            }
            else
            {
                for($i=0; $i<sizeof($this->select); $i++)
                {
                    $statement .= $this->select[$i];
                    
                    if($i != (sizeof($this->select)-1))
                    {
                        $statement .= ', ';
                    }
                }
            }
            
            return $statement;
        }
        
        private function getSqlLike()
        {
            $statement = '';
            
            if(isset($this->like))
            {
                if(!isset($this->where))
                {
                    $statement = ' WHERE ';
                }
                else
                {
                    $statement .= ' AND'; 
                }
            }
            
            return $statement . $this->like;
        }
        
        private function getSqlVar($var)
        {
            $statement = '';   
            
            if(isset($this->{$var}))
            {
                $statement .= ' ' . strtoupper($var) . ' ' . $this->{$var};
            }
            
            return $statement; 
        }
        
        private function getJoin()
        {
            if(isset($this->join))
            {
                 return $this->join;   
            }
            
            return '';
        }
        
        private function getSqlGroupBy()
        {
            $statement = '';

            if(isset($this->group))
            {
                $statement = ' GROUP BY ';
            
                for($i=0; $i<sizeof($this->group); $i++)
                {
                    $statement .= $this->group[$i];
                    
                    if($i != (sizeof($this->group)-1))
                    {
                        $statement .= ', ';   
                    }
                }
            }

            return $statement;
            
        }
        
        public function getSql()
        {
            return $this->sql;   
        }
        
        public function getLastQuery()
        {
            return $this->lastQuery;
        }

        public function getCurrentQuery()
        {
            $this->buildQuery();
            return $this->sql;
        }
        
        public function buildQuery()
        {
            $this->sql = $this->getSqlSelect();
            $this->sql .= ' FROM ' . $this->getTable();
            $this->sql .= $this->getJoin();
            $this->sql .= $this->getSqlVar('where');
            $this->sql .= $this->getSqlLike();
            $this->sql .= $this->getSqlVar('limit');
            $this->sql .= $this->getSqlVar('offset');
            $this->sql .= $this->getSqlVar('order');
            $this->sql .= $this->getSqlGroupBy();   
        }

        public function insert($param)
        {
            if(is_array($param))
            {
                $this->sql = 'INSERT INTO ' . $this->getTable();
                
                if($this->isAssoc($param))
                {
                    $valueString = '';
                    $this->sql .= ' ( ';
                    for($i=0; $i< sizeof($param); $i ++)
                    {
                        $this->sql .= key($param);
                        
                        $valueString .=  $this->addToPreparedValues($param[key($param)]);
                        
                        if($i != sizeof($param)-1)
                        {
                            $this->sql .= ' , ' ; 
                            $valueString .=' , ';
                        }
                        
                        next($param);
                    }
                    
                    $this->sql .= ' ) VALUES ( '. $valueString .' )';
                }
                else
                {
                    $this->sql .= ' VALUES ( ';
                    for($i=0; $i<sizeof($param); $i ++)
                    {  
                        
                        $this->sql .=  $this->addToPreparedValues($param[$i]);
                        
                        if($i != sizeof($param)-1)
                        {
                            $this->sql .= ' , ' ; 
                        }
                    }
                    
                    $this->sql .= ' )';
                }

                DB::runQuery($this->sql, $this->preparedStringValues, $this->preparedIntValues);
                $this->resetVariables();

            }
            else if(is_object($param))
            {
                $this->insert(get_object_vars($param));
            }
        }
        
        public function update($param, $where = null)
        {
        	if(isset($where))
        		$this->where($where);

            if(is_array($param))
            {
                if($this->isAssoc($param))
                {
                    $this->sql = 'UPDATE ' . $this->getTable() . ' SET ';
                        
                    for($i=0; $i< sizeof($param); $i ++)
                    {
                        $this->sql .= key($param) . '= ' . $this->addToPreparedValues($param[key($param)]);
                        
                        if($i != sizeof($param)-1)
                        {
                            $this->sql .= ' , ' ; 
                        }
                        
                        next($param);
                    }
                    
                    $this->sql .= $this->getSqlVar('where');

                    DB::runQuery($this->sql, $this->preparedStringValues, $this->preparedIntValues);
                    $this->resetVariables();
                }
            }
            else if(is_object($param))
            {
                $this->update(get_object_vars($param));
            }
        }
        
        public function delete($where = null)
        {
            
            if(isset($where))
            	$this->where($where);
            
            $this->sql = 'DELETE FROM ' . $this->getTable();
            
            if($this->where)
            {
                $this->sql .= $this->getSqlVar('where');
            }

            DB::runQuery($this->sql, $this->preparedStringValues, $this->preparedIntValues);
            $this->resetVariables();
        }

        public function get($class = null)
        {
        	$this->buildQuery();
        	$result = DB::runQuery($this->sql);

        	if(isset($class))
        	{
        		$result->setFetchMode(PDO::FETCH_CLASS,$class);
            	return $result->fetch();
        	}
        	else
        	{
           		return $this->prepareResult($result);
        	}
            
        }

        public function getInsertId()
        {
            return DB::getInsertId();   
        }

        public function getTableColumns($tableData = null)
        {
            if(isset($tableData))
            {
                if(is_string($tableData))
                {
                    return $this->prepareResult(DB::runQuery('SHOW COLUMNS FROM ' . $tableData));
                }
                else if(is_array($tableData))
                {
                    $final = array();
                   
                    for($i=0; $i<sizeof($tableData); $i++)
                    {
                        $result = $this->prepareResult(DB::runQuery('SHOW COLUMNS FROM ' . $tableData[$i]));
                        
                        $final = array_merge($final, $result);
                    }

                    return $final;
                }
            }
            else if(isset($this->table))
            {
                $data = $this->table;

                if(is_array($this->table))
                {
                    $data = array();
                    $data[] = $this->table[0];

                    for($i=1; $i<sizeof($this->table); $i++)
                    {
                        $data[] = $this->table[$i]['table'];
                    }
                }  

                return $this->getTableColumns($data);
            }
        }

        protected function checkTable($table = null)
        {
        	if(!isset($table))
        		$this->checkTable($this->table);
            else if(is_array($table))
            {
                $exist = false;

                if($this->getTableColumns($table[0]) !== null)
                {
                    $exist = true;   
                }

                $tempTable = $this->getTableColumns($table[0]);

                for($i=1; $i<sizeof($this->table); $i++)
                {
                    $checkTable = false;

                    foreach($tempTable as $column)
                    {
                        if($column['Field'] == $table[$i]['tableKey'])
                        {
                            $checkTable = true;
                        }

                        if($checkTable)
                        {
                            break;
                        }
                    }

                    $foreignTable = $this->getTableColumns($table[$i]['table']);
                    $checkForeign = false;

                    foreach($foreignTable as $foreignColumn)
                    {
                        if($foreignColumn['Field'] == $table[$i]['foreignKey'])
                        {
                            $checkForeign = true;
                        }

                        if($checkForeign)
                        {
                            break;
                        }
                    }
                }

                if(!$checkForeign || !$checkTable) 
                {
                    $exist = false;
                }

                if($exist)
                {
                    return true;
                }
            }
            else if(is_string($table))
            {
                if($this->getTableColumns($this->table) !== array())
                {
                    return true;
                }
            }

            return false;
            throw new Exception("There's a problem with your database keys.", 1);
        }

        public function getTable()
        {
            if(isset($this->table))
            {
                if(is_array($this->table))
                {
                    $statement = $this->table[0];
           
                    for($i=1; $i<sizeof($this->table); $i++)
                    {
                        if(isset($this->table[$i]['type']))
                        {
                            $statement .= ' ' . strtoupper($this->table[$i]['type']);
                        }

                        $statement .= ' JOIN ' . $this->table[$i]['table'] . ' ON ' . $this->table[$i]['tableKey'] . ' = '  . $this->table[$i]['foreignKey'];
                    }

                    return $statement;
                }
                else if(is_string($this->table))
                {
                    return $this->table;
                }
            }
            else
            	throw new Exception("Table is not set.", 1);
            	
        }

       private function addToPreparedValues($value)
       {
            $count = sizeof($this->preparedStringValues) + sizeof($this->preparedIntValues) +1;
           if(is_string($value))
           {
                $this->preparedStringValues[$count] = $value;
           }
           else if(is_int($value) || is_float($value))
           {
                $this->preparedIntValues[$count] = $value;
           }

           return ':' . ($count);
       }

       private  function prepareResult($data)
       {
            if(get_class($data) == 'PDOStatement')
            {
                $result = array();
            
                $result = $data->fetchAll(PDO::FETCH_ASSOC);

                if(sizeof($result) ==1)
                {
                    $newResult = $result[0];
                    
                    $result = $newResult;

                    if(sizeof($result) == 1)
                    {
                        return $result[key($result)];
                    }
                }
                else if(sizeof($result) == 0)
                {
                    $result = null;
                }
                
                return $result;
            }
            
            return null;
        }
	}
?>