<?php
require('config.php');
require('db.php');
require('query.php');
require('model.php');
require('usermodel.php');
class usermodelTest extends PHPUnit_Framework_TestCase
{
	/**
	*	
	*	
	*
	*	@dataProvider providerTestValidateemail
	*	
	*/
    public function testValidateemail($email)
	{
		
		try {
			$userModel = new UserModel();
			$this->assertTrue($userModel->validateemail($email));

		}
		catch(ValidationException $e)
		{
			echo $e->getMessage();
		}
	    
	}

	public function providerTestValidateemail()
	{
		return array(
			array('danielblignaut@gmail.com'), #test with an @ and a period
			array('danielbligaut@gmail.co.za'), #test with an @ and multiple periods
			array('DanielBlignaut@gmail.com'), #test case of letters
			array('daniel0189@gmail.com'), #test with numbers
			array('daniel!#$%&\'*+-/=?^_`{|}~@gmail.com'), #test with all allowed special characters
			array('danielblignaut@gmail'), #test with an @ 
			array('daniel.blignaut@gmail.com'), #test with period in local part
			array('daniel'), #test a normal string(error)
			array('daniel@gmail.com'), #not enough characters in local domain(error)
			array('.daniel@gmail.com'), #test with period in begining of local part(error)
			array('daniel.@gmail.com'), #test with period at end of local part(error)
			array('daniel..blignaut@gmail.com') #test with multiple periods (error)
		);
	}
}


?>