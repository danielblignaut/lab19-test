<?php
	class Model
	{
		protected $query;

		public function __construct()
		{
			try
			{
				if(isset(static::$table))
					$this->query = new query(static::$table);
				else
					throw new Exception("You haven't set your model's table.", 1);
			}
			catch(Exception $e)
			{
				echo $e->getMessage();
			}
		}

		public static function create($dataArr)
		{
			$instance = new Static;

			foreach($dataArr as $key => $value) 
			{
				//check if the variable actually exists
				if(property_exists(get_called_class(), $key))
				{
					//check if there is a set method
					if(method_exists(get_called_class(), 'set' . $key))
						$instance->{'set' . $key}($value); 
					else
						//will pick up an error if the variable is private. Will just try to set the variable normally.
						$instance->{$key} = $value;
				}
				else
					throw new Exception("Those variables don't exist", 1);
			}

				return $instance;
		}

		public function _save($id_name = 'id')
		{
			$attributes = (array) ($this);

			// Match columns with attributes
			$newAttributes = $this->matchColumns( $attributes );

			// If id is given then the user request an update to an
			// existing row		
			if(isset($newAttributes[$id_name]))
			{
				$this->query->where(array($id_name => $newAttributes[$id_name]));
				$this->query->update($newAttributes);
			}
			// Else then new row will be added
			else
			{
				$this->query->insert($newAttributes);
				$this->{$id_name} = $this->query->getInsertId();
			}
		}

		public function _delete($id_name = 'id')
		{
			$attributes = (array) ($this);
			//if id is set
			if(isset($attributes[$id_name]))
			{
				//for some reason, integer properties cast to array convert to strings.... so.
				if(is_numeric($attributes[$id_name]))
					$attributes[$id_name] = (int) $attributes[$id_name];
				$this->query->delete(array($id_name => $attributes[$id_name]));
			}
			else
				throw new Exception("ID of user is not set.", 1);
				
		}

		public static function first($id_name = 'name', $columns = '*', $class_name = null)
        {
        	$instance = new Static;
        	if(!isset($class_name))
        		$class_name = get_called_class();
        	$instance->query->select($columns);
        	$instance->query->order($id_name,'DESC');
            return $instance->query->get($class_name);
        }

		private function matchColumns($attributes)
		{
			// Make new array to hold all matching between
			// table columns names and attributes keys 
			$new = array();

			// Describe the table to get columns name
			
			$tableDetails = $this->query->getTableColumns();
			
			// Go through all columns
			foreach($tableDetails as $row)
			{
				// If this column name exist in attributes array 
				if(isset( $attributes[ $row['Field'] ] ))
					// put this value to the new array with its key
					$new[ $row['Field'] ] = $attributes[ $row['Field'] ];
			}
			return $new; 
		}

		public function __call($method, $args)
		{
			
			if(method_exists(get_called_class(), $method))
			{
				call_user_func_array($method, $args);
			}
			else
			{
				if(property_exists(get_called_class(), $method))
				{
					//they are trying to set the variable
					if(sizeof($args) == 1)
					{
						//check if any validation methods exist.
						if(method_exists(get_called_class(), 'validate' . $method))
						{
							if($this->{'validate' . $method}($args[0]))
								//passed validation, set field
								$this->{$method} = $args[0];
						}
						else
						{
							//no validation method, just set variable
							$this->{$method} = $args[0];	
						}
					}
					//they are trying to get the variable
					else if(sizeof($args) == 0)
						//return the variable
						return $this->{$method};
				}
			}	
		}

		
		
	}
?>